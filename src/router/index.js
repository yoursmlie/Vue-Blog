import Vue from 'vue'
import Router from 'vue-router'
import Blog from '@/components/AddBlog'
import ShowBlog from '@/components/ShowBlog'
import SingBlog from '@/components/SingBlog'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'Blog',
      component:  Blog
    },{
      path: '/showblog',
      name: 'ShowBlog',
      component: ShowBlog
    },{
      path: '/blog/:id',
      name: 'SingBlog',
      component: SingBlog
    }],
    mode: "history"
})
